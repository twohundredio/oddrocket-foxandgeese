#pragma once

#include <string>
#include <sstream>

// Workaround for NDK9
namespace patchndk
{
	template < typename T > std::string to_string(const T& n)
	{
		std::ostringstream stm;
		stm << n;
		return stm.str();
	}

	int stoi(const std::string& s);
}