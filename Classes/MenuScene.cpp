#include "MenuScene.h"
#include "GameScene.h"
#include "HelpScene.h"
#include "StatsScene.h"
#include "GameOptionsScene.h"
#include "SimpleButton.h"

USING_NS_CC;

Scene* MenuScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MenuScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	GameSettings::setupFontSize(visibleSize);

	int buttonWidth = visibleSize.width * 0.7;
	int buttonHeight = buttonWidth / 4;
	Vec2 pos;

	int numOfButtons = 4;
	int buttonSpacing = buttonHeight * 0.3;
	int buttonBlockHeight = (numOfButtons * buttonHeight) + ((numOfButtons - 1) * buttonSpacing);
	int buttonY = (visibleSize.height / 2) + (buttonBlockHeight / 2) - (buttonHeight / 2);

    // Background
	auto bgLayer = GameSettings::setBackgroundColour(this, origin, visibleSize, GameSettings::menuBackgroundColour);

	int fontSize = 62.0 / 480.0 * 720.0;
    auto label = Label::createWithTTF("Fox and Geese", GameSettings::titleFont, GameSettings::titleFontSize);
	label->setColor(GameSettings::titleColour);
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));
    this->addChild(label, 1);

	// Single player
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto singlePlayer = SimpleButton::create();
	singlePlayer->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	singlePlayer->setTitleText("One Player");
	singlePlayer->setTitleFontSize(GameSettings::buttonFontSize);
	singlePlayer->setTitleFontName(GameSettings::font);
	//singlePlayer->setTitleColor(GameSettings::buttonTextColour);
	singlePlayer->setZoomScale(0.1f);
	singlePlayer->setPosition(pos);
	singlePlayer->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
			TransitionFade::create(0.5, GameOptionsScene::createScene(true), Color3B(GameSettings::menuBackgroundColour)));
		}
	});

	this->addChild(singlePlayer);

	// Two player
	buttonY = buttonY - buttonHeight - buttonSpacing;
	pos = Vec2(origin.x + (visibleSize.width /2), buttonY);

	auto twoPlayer = SimpleButton::create();
	twoPlayer->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	twoPlayer->setTitleText("Two Player");
	twoPlayer->setTitleFontSize(GameSettings::buttonFontSize);
	twoPlayer->setTitleFontName(GameSettings::font);
	//twoPlayer->setTitleColor(GameSettings::buttonTextColour);
	twoPlayer->setZoomScale(0.1f);
	twoPlayer->setPosition(pos);
	twoPlayer->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, GameOptionsScene::createScene(false), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(twoPlayer);

	// Help
	buttonY = buttonY - buttonHeight - buttonSpacing;
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto helpButton = SimpleButton::create();
	helpButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	helpButton->setTitleText("Help");
	helpButton->setTitleFontSize(GameSettings::buttonFontSize);
	helpButton->setTitleFontName(GameSettings::font);
	//helpButton->setTitleColor(GameSettings::buttonTextColour);
	helpButton->setZoomScale(0.1f);
	helpButton->setPosition(pos);
	helpButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, HelpScene::createScene(), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(helpButton);

	// Stats
	buttonY = buttonY - buttonHeight - buttonSpacing;
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto statsButton = SimpleButton::create();
	statsButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	statsButton->setTitleText("Statistics");
	statsButton->setTitleFontSize(GameSettings::buttonFontSize);
	statsButton->setTitleFontName(GameSettings::font);
	//statsButton->setTitleColor(GameSettings::buttonTextColour);
	statsButton->setZoomScale(0.1f);
	statsButton->setPosition(pos);
	statsButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, StatsScene::createScene(), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(statsButton);
	
    return true;
}
