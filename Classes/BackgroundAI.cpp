#include "BackgroundAI.h"
#include "cocos2d.h"

// All funcitons called from same thread

BackgroundAI::BackgroundAI() : m_gen(0), m_moveAvailable(false)
{
}


BackgroundAI::~BackgroundAI()
{
}

void BackgroundAI::startAIMove(FGBoard &board)
{
	struct air_s *task = new struct air_s();

	task->gen = ++m_gen;
	task->board.copyState(board);

	m_moveAvailable = false;
	auto taskpool = cocos2d::AsyncTaskPool::getInstance();

	using std::placeholders::_1;
	std::function<void(void*)> callback = std::bind(&BackgroundAI::aiCallback, this, _1);

	taskpool->enqueue(cocos2d::AsyncTaskPool::TaskType::TASK_OTHER, callback, task,
		[task]() {
		if (task->board.isGeeseTurn())
		{
			task->result = task->board.moveGeese();
		}
		else
		{
			task->result = task->board.moveFox();
		}
	});
}

void BackgroundAI::aiCallback(void* v)
{
	struct air_s *task = (struct air_s*)v;

	if (task->gen == m_gen)
	{
		m_result = task->result;
		m_moveAvailable = true;
	}

	delete task;
}

bool BackgroundAI::moveAvailable()
{
	return m_moveAvailable;
}

FGBoard::Move BackgroundAI::getMove()
{
	m_moveAvailable = false;
	return m_result;
}