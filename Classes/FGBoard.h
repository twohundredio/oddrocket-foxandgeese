#pragma once

#include <vector>
#include <random>
#include <chrono>

class FGBoard
{
public:
	enum PieceType { NOGO, NONE, GEESE, FOX };
	enum MoveType { INVALID, MOVE, JUMP };
	enum MoveReason { OK, BOARD, TURN, DISTANCE, OCCUPIED, JUMPED, NOTHINGJUMPED, LINE };

	using MoveResult = struct MoveResult_t {
		MoveType type;
		MoveReason reason;
	};

	using Move = struct Move_t {
		bool valid;
		int sx;
		int sy;
		int ex;
		int ey;
		bool jump;
		int score;
	};

	FGBoard();
	~FGBoard();

	void setupBoard(int numberOfGeese);
	bool isGeeseTurn();
	PieceType getPeiceAt(int x, int y);
	MoveResult tryMove(int sx, int sy, int ex, int ey);
	Move moveFox();
	Move moveGeese();
	bool haveGeeseWon() const;
	bool hasFoxWon() const;
	bool foxCanEndTurn();
	void endTurn();
	bool isGameOver();
	int getNumberOfMoves();
	std::string getPlayTime();
	void copyState(FGBoard &fgboard);
    std::vector<double> getRepresentation();
    void setDifficulty(int difficulty);

private:
	static constexpr int AISCOREINFINITY = 50001;
	static constexpr int AISCOREWIN = 10000;

	enum PieceType board[7][7];
	bool geeseTurn;
	bool hasMoved;
	bool foxJumped;
	bool gameOver;
	int numberOfMoves;
	std::default_random_engine generator;
	std::chrono::time_point<std::chrono::system_clock> startTime, endTime;
    int m_difficulty;

	using XYPos = struct XYPos_t {
		int x;
		int y;
	};

	XYPos findFox() const;
	MoveResult isValidMove(int sx, int sy, int ex, int ey, bool isGeeseMove, bool foxJumped) const;
	void performMove(int sx, int sy, int ex, int ey);
	void performJump(int sx, int sy, int ex, int ey);
	std::vector<Move> getFoxMoves() const;
	std::vector<XYPos> findGeese() const;
	std::vector<Move> getGeeseMoves() const;
	bool foxHasJumps() const;
	int countGeese() const;
    bool evalCanMakeMove(FGBoard::PieceType evalboard[][7], int sx, int sy, int ex, int ey);
	int evalBoard(FGBoard &fgboard, std::vector<FGBoard::Move> &moves);
	std::vector<FGBoard::Move> minmax(FGBoard &fgboard, std::vector<FGBoard::Move> &moves, int depth);
    FGBoard::Move minmax_single(FGBoard &fgboard, std::vector<FGBoard::Move> &moves, int depth);
	unsigned int numberOfFoxMoves() const;
	unsigned int numberOfVulnerableGeese() const;
	double getFoxGeeseDistance() const;
	FGBoard::Move getAiMove();
};

