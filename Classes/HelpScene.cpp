#include <string>
#include "HelpScene.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "SimpleButton.h"

USING_NS_CC;
using std::string;

Scene* HelpScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelpScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool HelpScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	GameSettings::setupFontSize(visibleSize);

	string helpStr = "The object of the game for the geese is to\n" \
					 "capture the fox by surrounding him so he\n"	\
					 "has no possible moves or jumps.\n\n"			\
					 "The fox wins by capturing enough geese\n"		\
					 "so that not enough remain to capture him\n"	\
					 "(4 geese).\n\n"								\
					 "Players move one peice a turn to a vacant\n"	\
					 "ajecent space along the lines shown on the\n"	\
					 "board. The fox captures geese by jumping\n"	\
					 "over them, the jumped peice is removed.\n"	\
					 "The fox may capture more peices in the\n" \
					 "same turn if he can continue jumping.";

	int buttonWidth = visibleSize.width * 0.7;
	int buttonHeight = buttonWidth / 4;
	int buttonY = buttonHeight;

	// Background
	auto bgLayer = GameSettings::setBackgroundColour(this, origin, visibleSize, GameSettings::menuBackgroundColour);

	auto label = Label::createWithTTF("Help", GameSettings::titleFont, GameSettings::titleFontSize);
	label->setColor(GameSettings::titleColour);
	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height - label->getContentSize().height));
	this->addChild(label);

	float labelY;
	float labelX = (origin.x + 30);

	auto helpLabel = Label::createWithTTF(helpStr, GameSettings::font, GameSettings::fontSize);
	helpLabel->setAnchorPoint(Vec2(0, 1));
	helpLabel->setTextColor(Color4B::BLACK);
	labelY = (origin.y + visibleSize.height) - label->getContentSize().height * 2;
	helpLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(helpLabel);

	// Menu button
	Vec2 pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto menuButton = SimpleButton::create();
	menuButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	menuButton->setTitleText("Menu");
	menuButton->setTitleFontSize(GameSettings::buttonFontSize);
	menuButton->setTitleFontName(GameSettings::font);
	//menuButton->setTitleColor(GameSettings::buttonTextColour);
	menuButton->setZoomScale(0.1f);
	menuButton->setPosition(pos);
	menuButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, MenuScene::createScene(), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(menuButton);

	return true;
}
