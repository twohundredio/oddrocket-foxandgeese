#include "StatsScene.h"
#include "MenuScene.h"
#include "PatchNDK.h"
#include "GameSettings.h"
#include "SimpleButton.h"

USING_NS_CC;

const static std::string statsFile("fgstats.txt");

Scene* StatsScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = StatsScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

StatsScene::Stats StatsScene::getStats()
{
	Stats s;
	s.foxPlayed = 0;
	s.foxWon = 0;
	s.geesePlayed = 0;
	s.geeseWon = 0;

	std::string fp = FileUtils::getInstance()->getWritablePath();
	fp.append(statsFile);

	if (FileUtils::getInstance()->isFileExist(fp))
	{
		// Parse
		try
		{
			size_t pos = 0;
			std::string token;
			std::string sStr = FileUtils::getInstance()->getStringFromFile(fp);

			pos = sStr.find(",");
			token = sStr.substr(0, pos);
			s.geesePlayed = patchndk::stoi(token);
			sStr.erase(0, pos + 1);

			pos = sStr.find(",");
			token = sStr.substr(0, pos);
			s.geeseWon = patchndk::stoi(token);
			sStr.erase(0, pos + 1);

			pos = sStr.find(",");
			token = sStr.substr(0, pos);
			s.foxPlayed = patchndk::stoi(token);
			sStr.erase(0, pos + 1);

			s.foxWon = patchndk::stoi(sStr);
		}
		catch (...)
		{
			s.foxPlayed = 0;
			s.foxWon = 0;
			s.geesePlayed = 0;
			s.geeseWon = 0;
		}
	}

	return s;
}

void StatsScene::incGeesePlayed()
{
	Stats s = getStats();
	s.geesePlayed++;
	writeStats(s);
}

void StatsScene::incGeeseWon()
{
	Stats s = getStats();
	s.geeseWon++;
	writeStats(s);
}

void StatsScene::incFoxPlayed()
{
	Stats s = getStats();
	s.foxPlayed++;
	writeStats(s);
}

void StatsScene::incFoxWon()
{
	Stats s = getStats();
	s.foxWon++;
	writeStats(s);
}

void StatsScene::writeStats(Stats s)
{
	std::string fp = FileUtils::getInstance()->getWritablePath();
	fp.append(statsFile);

	std::stringstream ss;
	ss << s.geesePlayed << "," << s.geeseWon << "," << s.foxPlayed << "," << s.foxWon;

	FileUtils::getInstance()->writeStringToFile(ss.str(), fp);
}

bool StatsScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	GameSettings::setupFontSize(visibleSize);

	int buttonWidth = visibleSize.width * 0.7;
	int buttonHeight = buttonWidth / 4;
	int buttonY = buttonHeight;

	// Background
	auto bgLayer = GameSettings::setBackgroundColour(this, origin, visibleSize, GameSettings::menuBackgroundColour);

	auto label = Label::createWithTTF("Statistics", GameSettings::titleFont, GameSettings::titleFontSize);
	label->setColor(GameSettings::titleColour);
	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height - label->getContentSize().height));
	this->addChild(label, 1);

	// Labels
	Stats s = getStats();
	float labelY;
	float labelX = origin.x + visibleSize.width / 2;

	auto geesePlayedLabel = Label::createWithTTF("Games played as geese", GameSettings::font, GameSettings::fontSize);
	geesePlayedLabel->setAnchorPoint(Vec2(0.5, 0));
	geesePlayedLabel->setTextColor(Color4B::BLACK);
	labelY = (origin.y + visibleSize.height) - label->getContentSize().height * 3;
	geesePlayedLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(geesePlayedLabel, 1);

	auto geesePlayedValLabel = Label::createWithTTF(patchndk::to_string(s.geesePlayed), GameSettings::font, GameSettings::fontSize);
	geesePlayedValLabel->setAnchorPoint(Vec2(0.5, 0));
	geesePlayedValLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 1.5);
	geesePlayedValLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(geesePlayedValLabel, 1);

	auto geeseWonLabel = Label::createWithTTF("Games won as geese", GameSettings::font, GameSettings::fontSize);
	geeseWonLabel->setAnchorPoint(Vec2(0.5, 0));
	geeseWonLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 1.5);
	geeseWonLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(geeseWonLabel, 1);

	auto geeseWonValLabel = Label::createWithTTF(patchndk::to_string(s.geeseWon), GameSettings::font, GameSettings::fontSize);
	geeseWonValLabel->setAnchorPoint(Vec2(0.5, 0));
	geeseWonValLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 1.5);
	geeseWonValLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(geeseWonValLabel, 1);

	//Fox
	auto foxPlayedLabel = Label::createWithTTF("Games played as fox", GameSettings::font, GameSettings::fontSize);
	foxPlayedLabel->setAnchorPoint(Vec2(0.5, 0));
	foxPlayedLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 3);
	foxPlayedLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(foxPlayedLabel, 1);

	auto foxPlayedValLabel = Label::createWithTTF(patchndk::to_string(s.foxPlayed), GameSettings::font, GameSettings::fontSize);
	foxPlayedValLabel->setAnchorPoint(Vec2(0.5, 0));
	foxPlayedValLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 1.5);
	foxPlayedValLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(foxPlayedValLabel, 1);

	auto foxWonLabel = Label::createWithTTF("Games won as fox", GameSettings::font, GameSettings::fontSize);
	foxWonLabel->setAnchorPoint(Vec2(0.5, 0));
	foxWonLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 1.5);
	foxWonLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(foxWonLabel, 1);

	auto foxWonValLabel = Label::createWithTTF(patchndk::to_string(s.foxWon), GameSettings::font, GameSettings::fontSize);
	foxWonValLabel->setAnchorPoint(Vec2(0.5, 0));
	foxWonValLabel->setTextColor(Color4B::BLACK);
	labelY = labelY - (geesePlayedLabel->getContentSize().height * 1.5);
	foxWonValLabel->setPosition(Vec2(labelX, labelY));
	this->addChild(foxWonValLabel, 1);

	// Menu button
	Vec2 pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto menuButton = SimpleButton::create();
	menuButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	menuButton->setTitleText("Menu");
	menuButton->setTitleFontSize(GameSettings::buttonFontSize);
	menuButton->setTitleFontName(GameSettings::font);
	//menuButton->setTitleColor(GameSettings::buttonTextColour);
	menuButton->setZoomScale(0.1f);
	menuButton->setPosition(pos);
	menuButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, MenuScene::createScene(), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(menuButton);

	return true;
}
