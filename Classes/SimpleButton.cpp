#include "SimpleButton.h"

USING_NS_CC;

SimpleButton* SimpleButton::create()
{
	SimpleButton *widget = new (std::nothrow) SimpleButton;
	if (widget && widget->init())
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return nullptr;
}

void SimpleButton::addSimpleBackground(Color4B colour, float width, float height)
{
	_backgroundRenderer = LayerColor::create(colour, width, height);
	_backgroundRenderer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	addProtectedChild(_backgroundRenderer, -2, -1);
}

void SimpleButton::setBackgroundColour(Color4B colour)
{
	if (nullptr != _backgroundRenderer)
	{
		_backgroundRenderer->setColor(Color3B(colour));
		_backgroundRenderer->setOpacity(colour.a);
	}
}

Size SimpleButton::getVirtualRendererSize() const
{
	if (_unifySize)
	{
		return this->getNormalSize();
	}

	if (nullptr != _titleRenderer)
	{
		Size titleSize = _titleRenderer->getContentSize();
		if (!_normalTextureLoaded && _titleRenderer->getString().size() > 0)
		{
			if (_backgroundRenderer != nullptr)
			{
				Size bgSize = _backgroundRenderer->getContentSize();

				float width = titleSize.width > bgSize.width ? titleSize.width : bgSize.width;
				float height = titleSize.height > bgSize.height ? titleSize.height : bgSize.height;

				return Size(width, height);
			}
			else
			{
				return titleSize;
			}

		}
	}
	return _normalTextureSize;
}
