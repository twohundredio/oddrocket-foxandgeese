#include "PauseScene.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "GameSettings.h"
#include "SimpleButton.h"

USING_NS_CC;

static GameOptions gameOptions;

Scene* PauseScene::createScene(GameOptions options)
{
	gameOptions = options;

	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = PauseScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool PauseScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	GameSettings::setupFontSize(visibleSize);

	int buttonWidth = visibleSize.width * 0.7;
	int buttonHeight = buttonWidth / 4;
	Vec2 pos;

	int numOfButtons = 3;
	int buttonSpacing = buttonHeight * 0.3;
	int buttonBlockHeight = (numOfButtons * buttonHeight) + ((numOfButtons - 1) * buttonSpacing);
	int buttonY = (visibleSize.height / 2) + (buttonBlockHeight / 2) - (buttonHeight / 2);

	// Background
	auto bgLayer = GameSettings::setBackgroundColour(this, origin, visibleSize, GameSettings::menuBackgroundColour);

	auto label = Label::createWithTTF("Paused", GameSettings::titleFont, GameSettings::titleFontSize);
	label->setColor(GameSettings::titleColour);
	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height - label->getContentSize().height));
	this->addChild(label, 1);

	// Continue
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto continueButton = SimpleButton::create();
	continueButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	continueButton->setTitleText("Continue");
	continueButton->setTitleFontSize(GameSettings::buttonFontSize);
	continueButton->setTitleFontName(GameSettings::font);
	//continueButton->setTitleColor(GameSettings::buttonTextColour);
	continueButton->setZoomScale(0.1f);
	continueButton->setPosition(pos);
	continueButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			gameOptions.reset = false;
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, GameScene::createScene(gameOptions), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(continueButton);

	// Restart
	buttonY = buttonY - buttonHeight - buttonSpacing;
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);

	auto restartButton = SimpleButton::create();
	restartButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	restartButton->setTitleText("Restart");
	restartButton->setTitleFontSize(GameSettings::buttonFontSize);
	restartButton->setTitleFontName(GameSettings::font);
	//restartButton->setTitleColor(GameSettings::buttonTextColour);
	restartButton->setZoomScale(0.1f);
	restartButton->setPosition(pos);
	restartButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			gameOptions.reset = true;
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, GameScene::createScene(gameOptions), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(restartButton);

	// Finish
	buttonY = buttonY - buttonHeight - buttonSpacing;
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);
	
	auto finishButton = SimpleButton::create();
	finishButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	finishButton->setTitleText("Finish");
	finishButton->setTitleFontSize(GameSettings::buttonFontSize);
	finishButton->setTitleFontName(GameSettings::font);
	//finishButton->setTitleColor(GameSettings::buttonTextColour);
	finishButton->setZoomScale(0.1f);
	finishButton->setPosition(pos);
	finishButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, MenuScene::createScene(), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(finishButton);

	return true;
}
