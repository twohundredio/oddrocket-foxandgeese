#pragma once
#include <string>
#include "cocos2d.h"

class GameSettings
{
public:
	static const std::string titleFont;
	static const std::string font;
	static int titleFontSize;
	static int buttonFontSize;
	static int fontSize;
	static int optionButtonFontSize;
	static const cocos2d::Color4B menuBackgroundColour;
	static const cocos2d::Color4B gameBackgroundColour;
	static const cocos2d::Color4B selectedButtonColour;
	static const cocos2d::Color4B deselectedButtonColour;
	static const cocos2d::Color4B buttonColour;
	static const cocos2d::Color3B buttonTextColour;
	static const cocos2d::Color3B titleColour;

	GameSettings();
	~GameSettings();

	void loadSettings();
	void saveSettings();

	static void setupFontSize(cocos2d::Size visibleSize);

	bool soundEnabled();
	void setSoundEnabled(bool enabled);

	static cocos2d::LayerColor* setBackgroundColour(cocos2d::Layer *layer, cocos2d::Vec2 origin, cocos2d::Size visibleSize, cocos2d::Color4B colour);

private:
	static const std::string settingsFile;
	bool mSoundEnabled;
};

