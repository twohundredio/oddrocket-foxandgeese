#include "GameScene.h"
#include <iostream>
#include <cmath>
#include <cassert>
#include "SimpleAudioEngine.h"  
#include "PauseScene.h"
#include "StatsScene.h"
#include "MenuScene.h"
#include "PatchNDK.h"
#include "SimpleButton.h"
#include "BackgroundAI.h"

USING_NS_CC;
using std::vector;
using std::string;
using std::uniform_int_distribution;

static FGBoard fgboard;
static BackgroundAI backgroundAi;
static GameOptions gameOptions;
static bool scoreSet = false;

const Color4F GameScene::nodeColour = Color4F(Color4B(5, 73, 80, 255));
const Color4F GameScene::foxColour = Color4F(Color4B(247, 143, 30, 255));
const Color4F GameScene::geeseColour = Color4F(Color4B(237, 244, 245, 255));

Scene* GameScene::createScene(GameOptions options) {
	gameOptions = options;

	auto scene = Scene::create();
	auto layer = GameScene::create();
	scene->addChild(layer);

	return scene;
}

void GameScene::setNodeColour(cocos2d::DrawNode* node, cocos2d::Color4F colour)
{
	node->clear();
	node->drawDot(Vec2(0, 0), nodeRadius, colour);
}

void GameScene::updateBoard()
{
	// Add dots
	int i = 0;
	for (int y = 0; y < 7; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			bool toDraw = true;

			if ((x < 2 || x > 4) && (y < 2 || y > 4))
			{
				toDraw = false;
			}

			if (toDraw)
			{
				if (fgboard.getPeiceAt(x, y) == FGBoard::PieceType::FOX) {
					setNodeColour(nodes.at(i), foxColour);
				}
				else if (fgboard.getPeiceAt(x, y) == FGBoard::PieceType::GEESE) {
					setNodeColour(nodes.at(i), geeseColour);
				}
				else if (fgboard.getPeiceAt(x, y) == FGBoard::PieceType::NONE) {
					setNodeColour(nodes.at(i), nodeColour);
				}
				i++;
			}
		}
	}

	if (fgboard.isGameOver())
	{
		setTurnButtonVisable(false);
		setFinishButtonVisable(true);
		if (this->isScheduled(schedule_selector(GameScene::foxCallback)))
		{
			this->unschedule(schedule_selector(GameScene::foxCallback));
		}
		if (this->isScheduled(schedule_selector(GameScene::geeseCallback)))
		{
			this->unschedule(schedule_selector(GameScene::geeseCallback));
		}

		if (fgboard.hasFoxWon())
		{
			std::string gwin("Fox wins!");
			turnLabel->setString(gwin);
			showMessage(gwin);

			if (!scoreSet)
			{
				if (!foxAi && geeseAi)
				{
					scoreSet = true;
					StatsScene::incFoxWon();
				}
			}
		} 
		else
		{
			std::string gwin("Geese win!");
			turnLabel->setString(gwin);
			showMessage(gwin);

			if (!scoreSet)
			{
				if (foxAi && !geeseAi)
				{
					scoreSet = true;
					StatsScene::incGeeseWon();
				}
			}
		}
	}
	else if (fgboard.isGeeseTurn())
	{
		setNodeColour(moveNode, geeseColour);
		setFinishButtonVisable(false);

		if (foxAi && !geeseAi)
		{
			turnLabel->setString("Your turn...");
		} 
		else
		{
			if (geeseAi)
			{
				turnLabel->setString("Geese's turn... thinking");
			}
			else
			{
				turnLabel->setString("Geese's turn...");
			}
		}

		setTurnButtonVisable(false);

		if (geeseAi)
		{
			if (this->isScheduled(schedule_selector(GameScene::foxCallback)))
			{
				this->unschedule(schedule_selector(GameScene::foxCallback));
			}

			backgroundAi.startAIMove(fgboard);

			if (!this->isScheduled(schedule_selector(GameScene::geeseCallback)))
			{
				this->schedule(schedule_selector(GameScene::geeseCallback), minMoveTime);
			}
		}
	}
	else if (!fgboard.isGeeseTurn()) 
	{
		setNodeColour(moveNode, foxColour);
		setFinishButtonVisable(false);

		if (!foxAi && geeseAi)
		{
			turnLabel->setString("Your turn...");
		}
		else
		{
			if (foxAi)
			{
				turnLabel->setString("Fox's turn... thinking");
			}
			else
			{
				turnLabel->setString("Fox's turn...");
			}
		}

		if (!foxAi)
		{
			if (fgboard.foxCanEndTurn())
			{
				setTurnButtonVisable(true);
			}
			else
			{
				setTurnButtonVisable(false);
			}
		}
		else
		{
			if (this->isScheduled(schedule_selector(GameScene::geeseCallback)))
			{
				this->unschedule(schedule_selector(GameScene::geeseCallback));
			}

			backgroundAi.startAIMove(fgboard);

			if (!this->isScheduled(schedule_selector(GameScene::foxCallback)))
			{
				this->schedule(schedule_selector(GameScene::foxCallback), minMoveTime);
			}
		}
	}
}

void GameScene::geeseCallback(float dt)
{
	if (backgroundAi.moveAvailable()) {
		this->unschedule(schedule_selector(GameScene::geeseCallback));
		turnLabel->setString("Geese's turn... moving");
		animateMove = backgroundAi.getMove();
		FGBoard::MoveResult r = fgboard.tryMove(animateMove.sx, animateMove.sy, animateMove.ex, animateMove.ey);
		assert(r.type != FGBoard::MoveType::INVALID);
		startAnimation();
	}
}


void GameScene::foxCallback(float dt)
{
	if (backgroundAi.moveAvailable()) {
		this->unschedule(schedule_selector(GameScene::foxCallback));
		turnLabel->setString("Fox's turn... moving");
		animateMove = backgroundAi.getMove();
		FGBoard::MoveResult r = fgboard.tryMove(animateMove.sx, animateMove.sy, animateMove.ex, animateMove.ey);
		assert(r.type != FGBoard::MoveType::INVALID);
		startAnimation();
	}
}

void GameScene::startAnimation()
{
	showingAnimation = true;
	animationStep = 0;
	this->schedule(schedule_selector(GameScene::animateCallback), 0.05f);
}

void GameScene::animateCallback(float dt)
{
	// moveNode
	int nodeStartIdx = getIndexFromXY(animateMove.sx, animateMove.sy);
	int nodeEndIdx = getIndexFromXY(animateMove.ex, animateMove.ey);
	auto nodeStart = nodes.at(nodeStartIdx);
	auto nodeEnd = nodes.at(nodeEndIdx);
	int steps = animateMove.jump ? 40 : 20;

	if (animationStep == 0)
	{
		moveNode->setPosition(nodeStart->getPosition());
		moveNode->setVisible(true);

		setNodeColour(nodeStart, nodeColour);
	} 
	else
	{
		auto pos = moveNode->getPosition();
		
		float stepx = (nodeEnd->getPositionX() - nodeStart->getPositionX()) / (float)steps;
		float stepy = (nodeEnd->getPositionY() - nodeStart->getPositionY()) / (float)steps;

		pos.x += stepx;
		pos.y += stepy;

		moveNode->setPosition(pos);
	}

	if (animateMove.jump && animationStep == (steps/2))
	{
		int xj = animateMove.sx + ((animateMove.ex - animateMove.sx) / 2);
		int yj = animateMove.sy + ((animateMove.ey - animateMove.sy) / 2);
		int nodeJumpIdx = getIndexFromXY(xj, yj);
		setNodeColour(nodes.at(nodeJumpIdx), nodeColour);
	}

	animationStep++;

	if (animationStep > steps)
	{
		if (this->isScheduled(schedule_selector(GameScene::animateCallback)))
		{
			this->unschedule(schedule_selector(GameScene::animateCallback));
		}

		playMoveSound();
		animationStep = 0;
		showingAnimation = false;
		moveNode->setVisible(false);
		updateBoard();
	}
}

void GameScene::drawBoard(float x, float y, float size)
{
	size = size - nodeRadius * 2;
	x = x + nodeRadius;
	y = y + nodeRadius;

	float length = size;
	float linc = length / 6;
	
	for (int r = 0; r < 7; r++) 
	{
		auto drawL1 = DrawNode::create();
		float xl = x;
		float yl = y + (r * linc);
		float linelen;
		switch (r)
		{
			case 0:
			case 1:
			case 5:
			case 6:
				linelen = length / 3;
				xl = xl + linelen;
				break;
			default:
				linelen = length;
		}
		drawL1->drawLine(Vec2(xl, yl), Vec2(xl + linelen, yl), nodeColour);
		this->addChild(drawL1);
	}

	for (int c = 0; c < 7; c++)
	{
		auto drawL1 = DrawNode::create();
		float xl = x + (c * linc);
		float yl = y;
		float linelen;
		switch (c)
		{
		case 0:
		case 1:
		case 5:
		case 6:
			linelen = length / 3;
			yl = yl + linelen;
			break;
		default:
			linelen = length;
		}
		drawL1->drawLine(Vec2(xl, yl), Vec2(xl, yl + linelen), nodeColour);
		this->addChild(drawL1);
	}

	float xs;
	float ys;
	float xe;
	float ye;

	xs = x + (4 * linc);
	ys = y;
	xe = x;
	ye = y + (4 * linc);
	auto drawL1 = DrawNode::create();
	drawL1->drawLine(Vec2(xs, ys), Vec2(xe, ye), nodeColour);
	this->addChild(drawL1);

	xs = x + (6 * linc);
	ys = y + (2 * linc);
	xe = x + (2 * linc);
	ye = y + (6 * linc);
	auto drawL2 = DrawNode::create();
	drawL2->drawLine(Vec2(xs, ys), Vec2(xe, ye), nodeColour);
	this->addChild(drawL2);

	xs = x + (4 * linc);
	ys = y + (2 * linc);
	xe = x + (2 * linc);
	ye = y + (4 * linc);
	auto drawL3 = DrawNode::create();
	drawL3->drawLine(Vec2(xs, ys), Vec2(xe, ye), nodeColour);
	this->addChild(drawL3);	

	xs = x + (0 * linc);
	ys = y + (2 * linc);
	xe = x + (4 * linc);
	ye = y + (6 * linc);
	auto drawL4 = DrawNode::create();
	drawL4->drawLine(Vec2(xs, ys), Vec2(xe, ye), nodeColour);
	this->addChild(drawL4);

	xs = x + (2 * linc);
	ys = y + (2 * linc);
	xe = x + (4 * linc);
	ye = y + (4 * linc);
	auto drawL5 = DrawNode::create();
	drawL5->drawLine(Vec2(xs, ys), Vec2(xe, ye), nodeColour);
	this->addChild(drawL5);

	xs = x + (2 * linc);
	ys = y + (0 * linc);
	xe = x + (6 * linc);
	ye = y + (4 * linc);
	auto drawL6 = DrawNode::create();
	drawL6->drawLine(Vec2(xs, ys), Vec2(xe, ye), nodeColour);
	this->addChild(drawL6);

	// Add dots
	for (int r = 0; r < 7; r++)
	{
		for (int c = 0; c < 7; c++)
		{
			bool toDraw = true;
			float xl = x + (c * linc);
			float yl = y + (r * linc);

			if ((r < 2 || r > 4) && (c < 2 || c > 4))
			{
				toDraw = false;
			}

			if (toDraw)
			{
				auto drawC = DrawNode::create();
				drawC->drawDot(Vec2(0, 0), nodeRadius, nodeColour);
				this->addChild(drawC);

				nodes.push_back(drawC);
				drawC->setPosition(Vec2(xl, yl));
			}
		}
	}

	moveNode = DrawNode::create();
	moveNode->drawDot(Vec2(0, 0), nodeRadius, nodeColour);
	moveNode->setVisible(false);
	this->addChild(moveNode);
}

bool GameScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	selected = -1;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	GameSettings::setupFontSize(visibleSize);

	random_engine.seed(random_device());

	sounds.push_back("sound1.wav");
	sounds.push_back("sound2.wav");
	sounds.push_back("sound3.wav");
	sounds.push_back("sound4.wav");
	sounds.push_back("sound5.wav");
	sounds.push_back("sound6.wav");
	sounds.push_back("sound7.wav");

	// Background
	auto bgLayer = GameSettings::setBackgroundColour(this, origin, visibleSize, GameSettings::gameBackgroundColour);
	
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(GameScene::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
	int BOARDSIZE = visibleSize.width * 0.95;
	int boardX = (origin.x + visibleSize.width / 2) - (BOARDSIZE / 2);
	drawBoard(boardX, (origin.y + visibleSize.height / 2) - (BOARDSIZE / 2) + 84, BOARDSIZE);
	
	float iconbuttonX = origin.x + visibleSize.width - 24 - 14;
	float iconbuttonY = origin.y + visibleSize.height - 24 - 14;
	auto menuButton = ui::Button::create("icon-menu.png", "icon-menu.png", "icon-menu.png");
	menuButton->setPosition(Vec2(iconbuttonX, iconbuttonY));
	menuButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, PauseScene::createScene(gameOptions), Color3B(GameSettings::gameBackgroundColour)));
		}
	});
	this->addChild(menuButton);

	iconbuttonX = iconbuttonX - 48 - 14;
	soundButton = ui::Button::create("icon-vol-mute.png", "icon-vol-mute.png", "icon-vol-loud.png");
	soundButton->setPosition(Vec2(iconbuttonX, iconbuttonY));
	soundButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			settings.setSoundEnabled(!settings.soundEnabled());
			settings.saveSettings();
			soundButton->setBright(!settings.soundEnabled());
		}
	});
	this->addChild(soundButton);

	// Message pane
	Vec2 msgPos = Vec2(origin.x + visibleSize.width / 2, origin.y + (visibleSize.height - 60 / 2));

	msgLayer = LayerColor::create(Color4B(foxColour), visibleSize.width, 60);
	msgLayer->setAnchorPoint(Vec2(0.5, 0.5));
	msgLayer->ignoreAnchorPointForPosition(false);
	msgLayer->setPosition(msgPos);
	this->addChild(msgLayer);

	msgLabel = Label::createWithTTF("Message goes here", GameSettings::font, GameSettings::fontSize);
	msgLabel->setAnchorPoint(Vec2(0.5, 0.5));
	msgLabel->setTextColor(Color4B::BLACK);
	msgLabel->setPosition(msgPos);
	this->addChild(msgLabel, 1);

	msgLayer->setVisible(false);
	msgLabel->setVisible(false);

	// Labels
	turnLabel = Label::createWithTTF("Finish Turn", GameSettings::font, GameSettings::fontSize);
	turnLabel->setAnchorPoint(Vec2(0, 0.5));
	turnLabel->setTextColor(Color4B::BLACK);
	float turnLabelY = (origin.y + visibleSize.height / 2) - (BOARDSIZE / 2) - (turnLabel->getContentSize().height * 1.5) + 62;
	turnLabel->setPosition(Vec2((origin.x + visibleSize.width / 2) - (BOARDSIZE / 2),
		turnLabelY));
	this->addChild(turnLabel, 1);

	// Next turn button
	float buttonWidth = turnLabel->getContentSize().width * 1.5;
	float buttonHeight = turnLabel->getContentSize().height * 1.5;
	float buttonX = boardX + BOARDSIZE - (buttonWidth / 2);
	Vec2 pos = Vec2(buttonX, turnLabelY);

	turnButton = SimpleButton::create();
	turnButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	turnButton->setTitleText("Finish Turn");
	turnButton->setTitleFontSize(GameSettings::fontSize);
	turnButton->setTitleFontName(GameSettings::font);
	turnButton->setZoomScale(0.1f);
	turnButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			fgboard.endTurn();
			updateBoard();
		}
	});
	turnButton->setPosition(pos);
	this->addChild(turnButton);
	setTurnButtonVisable(false);

	// Finish counts
	turnCountLabel = Label::createWithTTF("Number of turns: ", GameSettings::font, GameSettings::fontSize);
	turnCountLabel->setAnchorPoint(Vec2(0, 0.5));
	turnCountLabel->setTextColor(Color4B::BLACK);
	turnLabelY = turnLabelY  - (turnLabel->getContentSize().height * 1.5);
	turnCountLabel->setPosition(Vec2((origin.x + visibleSize.width / 2) - (BOARDSIZE / 2), turnLabelY));
	this->addChild(turnCountLabel, 1);

	timeCountLabel = Label::createWithTTF("Play time: ", GameSettings::font, GameSettings::fontSize);
	timeCountLabel->setAnchorPoint(Vec2(0, 0.5));
	timeCountLabel->setTextColor(Color4B::BLACK);
	turnLabelY = turnLabelY - (turnLabel->getContentSize().height * 1.5);
	timeCountLabel->setPosition(Vec2((origin.x + visibleSize.width / 2) - (BOARDSIZE / 2), turnLabelY));
	this->addChild(timeCountLabel, 1);

	// Finish button
	turnLabelY = turnLabelY - (turnLabel->getContentSize().height * 2.5);
	pos = Vec2(buttonX, turnLabelY);

	finishButton = SimpleButton::create();
	finishButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	finishButton->setTitleText("Finish");
	finishButton->setTitleFontSize(GameSettings::fontSize);
	finishButton->setTitleFontName(GameSettings::font);
	finishButton->setZoomScale(0.1f);
	finishButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, MenuScene::createScene(), Color3B(GameSettings::gameBackgroundColour)));
		}
	});
	finishButton->setPosition(pos);
	this->addChild(finishButton);

	setFinishButtonVisable(false);

	// Setup board
	settings.loadSettings();
	soundButton->setBright(!settings.soundEnabled());

	foxAi = gameOptions.foxAi;
	geeseAi = gameOptions.geeseAi;
	showingAnimation = false;

	if (gameOptions.reset)
	{
		fgboard.setupBoard(gameOptions.numberOfGeese);
        fgboard.setDifficulty(gameOptions.difficulty);

		if (geeseAi && !foxAi) {
			StatsScene::incFoxPlayed();
		} 
		else if (foxAi && !geeseAi)
		{
			StatsScene::incGeesePlayed();
		}

		scoreSet = false;
	}

	// Update display
	updateBoard();

	return true;
}

void GameScene::setTurnButtonVisable(bool visable)
{
	turnButton->setVisible(visable);
}

void GameScene::setFinishButtonVisable(bool visable)
{
	turnCountLabel->setVisible(visable);
	timeCountLabel->setVisible(visable);
	finishButton->setVisible(visable);

	if (visable)
	{
		std::string turns("Number of moves: ");
		turns.append(patchndk::to_string(fgboard.getNumberOfMoves()));
		turnCountLabel->setString(turns);

		std::string time("Play time: ");
		time.append(fgboard.getPlayTime());
		timeCountLabel->setString(time);
	}
}

Vec2 GameScene::getXYFromIndex(int i)
{
	int x = 0;
	int y = 0;
	if (i <= 5)
	{
		x = 2 + (i % 3);
		y = i / 3;
	}
	else if (i >= 27)
	{
		i = i - 27;
		x = 2 + (i % 3);
		y = 5 + (i / 3);
	}
	else
	{
		i = i - 6;
		x = i % 7;
		y = 2 + (i / 7);
	}

	return Vec2(x, y);
}

int GameScene::getIndexFromXY(int x, int y)
{
	int index = 0;
	if (y < 2)
	{
		index = (x - 2) + (y * 3);
	}
	else if (y < 5) 
	{
		index = 6;
		index += x + ((y - 2) * 7);
	} 
	else
	{
		index = 27;
		index += (x - 2) + ((y - 5) * 3);
	}
	return index;
}

int GameScene::getNodeIdxAt(Vec2 location, float intersectRadius)
{
	int i = 0;

	for (auto n : nodes)
	{
		float x1 = n->getPositionX();
		float y1 = n->getPositionY();

		float x2 = location.x;
		float y2 = location.y;

		if (sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2)) <= (intersectRadius + nodeRadius))
		{
			return i;
		}

		i++;
	}

	return -1;
}

void GameScene::showMoveResult(FGBoard::MoveResult result)
{
	std::string msg("Invalid move");
	if (result.reason == FGBoard::MoveReason::BOARD)
	{
		msg = "Move not on board!";
	}
	else if (result.reason == FGBoard::MoveReason::OCCUPIED)
	{
		msg = "Can only move to free spaces";
	}
	else if (result.reason == FGBoard::MoveReason::LINE)
	{
		msg = "Can only move along lines";
	}
	else if (result.reason == FGBoard::MoveReason::DISTANCE)
	{
		if (!fgboard.isGeeseTurn())
		{
			msg = "Can only move one space or two when jumping";
		} 
		else
		{
			msg = "Can only move one space";
		}
	}
	else if (result.reason == FGBoard::MoveReason::TURN)
	{
		msg = "Not your turn";
	}
	else if (result.reason == FGBoard::MoveReason::JUMPED)
	{
		msg = "Move must be a jump";
	}
	else if (result.reason == FGBoard::MoveReason::NOTHINGJUMPED)
	{
		msg = "No geese to jump";
	}
	showMessage(msg);
}

bool GameScene::onTouchBegan(Touch* touch, Event* event)
{
	auto location = touch->getLocation();
	int i = 0;
	i = getNodeIdxAt(location, 2);

	if ((fgboard.isGeeseTurn() && geeseAi) ||
		(!fgboard.isGeeseTurn() && foxAi))
	{
		if (i != -1)
		{
			std::string msg("Not your turn");
			showMessage(msg);
		}
		return false;
	}

	if (showingAnimation || fgboard.isGameOver())
	{
		return false;
	}

	if (i != -1)
	{
		Vec2 pos = getXYFromIndex(i);
		int x = pos.x;
		int y = pos.y;

		if (selected != -1 && 
			!(fgboard.isGeeseTurn() && fgboard.getPeiceAt(x, y) == FGBoard::PieceType::GEESE))
		{
			if (selected != i)
			{
				Vec2 spos = getXYFromIndex(selected);
				int sx = spos.x;
				int sy = spos.y;

				FGBoard::MoveResult result = fgboard.tryMove(sx, sy, x, y);
				if (result.type != FGBoard::MoveType::INVALID)
				{
					playMoveSound();
					updateBoard();
					selected = -1;
				}
				else
				{
					showMoveResult(result);
				}
			}
			else
			{
				selected = -1;
			}
		}
		else
		{
			selected = -1;

			if (fgboard.getPeiceAt(x, y) != FGBoard::PieceType::NONE)
			{
				if ((fgboard.isGeeseTurn() && fgboard.getPeiceAt(x, y) == FGBoard::PieceType::GEESE) ||
					(!fgboard.isGeeseTurn() && fgboard.getPeiceAt(x, y) == FGBoard::PieceType::FOX))
				{
					selected = i;
				}
				else
				{
					std::string msg;
					if (fgboard.isGeeseTurn())
					{
						msg = "Geese's turn";
					}
					else
					{
						msg = "Fox's turn";
					}
					showMessage(msg);
				}
			}
		}

		return true;
	}

	return false;
}

void GameScene::onTouchEnded(Touch* touch, Event* event)
{
	auto location = touch->getLocation();
	int i;

	moveNode->setVisible(false);

	i = getNodeIdxAt(location, nodeRadius);

	if (selected != -1)
	{
		if (i != -1)
		{
			Vec2 pos = getXYFromIndex(i);
			int x = pos.x;
			int y = pos.y;

			if (selected != i)
			{
				Vec2 spos = getXYFromIndex(selected);
				int sx = spos.x;
				int sy = spos.y;

				FGBoard::MoveResult result = fgboard.tryMove(sx, sy, x, y);
				if (result.type != FGBoard::MoveType::INVALID)
				{
					playMoveSound();
				}
				else
				{
					showMoveResult(result);
				}

			}
		}

		if (selected != i)
		{
			selected = -1;
		}
		updateBoard();
	}
}

void GameScene::onTouchMoved(Touch* touch, Event* event)
{
	if (selected != -1)
	{
		moveNode->setVisible(true);
		moveNode->setPosition(touch->getLocation());
		setNodeColour(nodes.at(selected), nodeColour);
	}
}

void GameScene::onTouchCancelled(Touch* touch, Event* event)
{

}

void GameScene::playMoveSound()
{
	std::uniform_int_distribution<int> dist(0, sounds.size() - 1);
	string sound = sounds[dist(random_engine)];

	if (settings.soundEnabled())
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(sound.c_str());
	}
}

void GameScene::showMessage(std::string& msg)
{
	msgLabel->setString(msg);
	messageStep = 0;
	this->schedule(schedule_selector(GameScene::messageCallback), 0.05f);
}

void GameScene::messageCallback(float dt)
{
	int steps = 64;
	int fadesteps = 16;

	if (messageStep == 0)
	{
		msgLayer->setVisible(true);
		msgLabel->setVisible(true);
		msgLayer->setOpacity(255);
		msgLabel->setOpacity(255);
	}

	messageStep++;

	if (messageStep >= (steps - fadesteps))
	{
		float op = 255.0 - (255.0 * (float)(messageStep - (steps - fadesteps)) / (float)fadesteps);
		msgLayer->setOpacity(op);
		msgLabel->setOpacity(op);
	}

	if (messageStep > steps)
	{
		if (this->isScheduled(schedule_selector(GameScene::messageCallback)))
		{
			this->unschedule(schedule_selector(GameScene::messageCallback));
		}

		messageStep = 0;
		msgLayer->setVisible(false);
		msgLabel->setVisible(false);
	}
}