#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class StatsScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	static void incGeesePlayed();
	static void incGeeseWon();
	static void incFoxPlayed();
	static void incFoxWon();
	CREATE_FUNC(StatsScene);

private:
	using Stats = struct stats
	{
		unsigned int foxPlayed;
		unsigned int foxWon;
		unsigned int geesePlayed;
		unsigned int geeseWon;
	};
	static Stats getStats();
	static void writeStats(Stats s);
};

