#include "cocos2d.h"
#include "GameSettings.h"

USING_NS_CC;
using namespace std;

const string GameSettings::settingsFile("fgsettings.txt");
const string GameSettings::titleFont("fonts/sketchtica.ttf");
const string GameSettings::font("fonts/GeosansLight.ttf");
const cocos2d::Color4B GameSettings::menuBackgroundColour(247, 143, 0, 255);
const cocos2d::Color4B GameSettings::gameBackgroundColour(120, 154, 159, 255);
const cocos2d::Color4B GameSettings::selectedButtonColour(0, 0, 0, 64);
const cocos2d::Color4B GameSettings::deselectedButtonColour(0, 0, 0, 16);
const cocos2d::Color4B GameSettings::buttonColour(0, 0, 0, 64);
const cocos2d::Color3B GameSettings::titleColour(Color3B::BLACK);
const cocos2d::Color3B GameSettings::buttonTextColour(Color3B::WHITE);
int GameSettings::titleFontSize(62);
int GameSettings::buttonFontSize(40);
int GameSettings::fontSize(24);
int GameSettings::optionButtonFontSize(32);

GameSettings::GameSettings() : mSoundEnabled(true)
{
}


GameSettings::~GameSettings()
{
}

void GameSettings::setupFontSize(cocos2d::Size visibleSize)
{
	GameSettings::titleFontSize = 62.0 / 480.0 * (double)visibleSize.width;
	GameSettings::buttonFontSize = 40.0 / 480.0 * (double)visibleSize.width;
	GameSettings::fontSize = 24.0 / 480.0 * (double)visibleSize.width;
	GameSettings::optionButtonFontSize = 32.0 / 480.0 * (double)visibleSize.width;
}

void GameSettings::loadSettings()
{
	string fp = FileUtils::getInstance()->getWritablePath();
	fp.append(settingsFile);

	if (FileUtils::getInstance()->isFileExist(fp))
	{
		string sStr = FileUtils::getInstance()->getStringFromFile(fp);
		if (sStr == "true")
		{
			mSoundEnabled = true;
		}
		else
		{
			mSoundEnabled = false;
		}
	}
}

void GameSettings::saveSettings()
{
	string fp = FileUtils::getInstance()->getWritablePath();
	fp.append(settingsFile);
	string sStr;

	if (mSoundEnabled)
	{
		sStr = "true";
	}
	else
	{
		sStr = "false";
	}

	FileUtils::getInstance()->writeStringToFile(sStr, fp);
}

bool GameSettings::soundEnabled()
{
	return mSoundEnabled;
}

void GameSettings::setSoundEnabled(bool enabled)
{
	mSoundEnabled = enabled;
}

LayerColor* GameSettings::setBackgroundColour(Layer *layer, Vec2 origin, Size visibleSize, Color4B colour)
{
	auto bgLayer = LayerColor::create(colour, visibleSize.width, visibleSize.height);
	bgLayer->setAnchorPoint(Vec2(0.5, 0.5));
	bgLayer->ignoreAnchorPointForPosition(false);
	bgLayer->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	layer->addChild(bgLayer);

	return bgLayer;
}