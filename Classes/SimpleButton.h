#pragma once
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class SimpleButton : public cocos2d::ui::Button
{
public:
	static SimpleButton* create();
	void addSimpleBackground(cocos2d::Color4B colour, float width, float height);
	void setBackgroundColour(cocos2d::Color4B colour);

protected:
	cocos2d::LayerColor *_backgroundRenderer = nullptr;
	virtual cocos2d::Size getVirtualRendererSize() const;
};

