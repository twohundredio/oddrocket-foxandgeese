#include "GameOptionsScene.h"
#include "GameScene.h"
#include "GameSettings.h"
#include "SimpleButton.h"

USING_NS_CC;

static bool m_gameSingle;

Scene* GameOptionsScene::createScene(bool singlePlayer)
{
	m_gameSingle = singlePlayer;

	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameOptionsScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool GameOptionsScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	GameSettings::setupFontSize(visibleSize);

	if (m_gameSingle)
	{
		options.difficulty = 1;
		options.geeseAi = false;
		options.foxAi = true;

        /* XXX */
        //options.geeseAi = true;
        //options.foxAi = true;
	}
	else
	{
		options.geeseAi = false;
		options.foxAi = false;
	}

	float buttonWidth = visibleSize.width * 0.7;
	float buttonHeight = buttonWidth / 4;

	int buttonY = buttonHeight;
	Vec2 pos;
	int yPos = 0;
	int spacing = visibleSize.width / 24;
	float optionButtonHeight = buttonHeight * 0.8;
	int optionButtonFontSize = GameSettings::optionButtonFontSize;
	const Color4B subHeadingColour(Color4B(5, 73, 80, 255));
	const Color3B buttonTextColour(Color4B(0, 0, 0, 255));

	// Background
	auto bgLayer = GameSettings::setBackgroundColour(this, origin, visibleSize, GameSettings::menuBackgroundColour);

	auto label = Label::createWithTTF("Game Options", GameSettings::titleFont, GameSettings::titleFontSize);
	label->setColor(GameSettings::titleColour);
	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height - label->getContentSize().height));
	this->addChild(label, 1);

	yPos = origin.y + visibleSize.height - label->getContentSize().height * 2.25;

	// Play as
	auto playAsLabel = Label::createWithTTF("Play As", GameSettings::font, GameSettings::buttonFontSize);
	playAsLabel->setPosition(Vec2(origin.x + visibleSize.width / 2, yPos));
	playAsLabel->setTextColor(subHeadingColour);
	this->addChild(playAsLabel, 1);

	yPos = yPos - playAsLabel->getContentSize().height - (optionButtonHeight / 2);

	float playAsButtonWidth = ((visibleSize.width * 0.9) - (spacing * 1)) / 2;
	float playAsButtonX = origin.x + (((visibleSize.width * 0.1) + playAsButtonWidth) / 2);
	pos = Vec2(playAsButtonX, yPos);

	geeseButton = SimpleButton::create();
	geeseButton->addSimpleBackground(GameSettings::selectedButtonColour, playAsButtonWidth, optionButtonHeight);
	geeseButton->setTitleText("Geese");
	geeseButton->setTitleFontSize(optionButtonFontSize);
	geeseButton->setTitleFontName(GameSettings::font);
	geeseButton->setTitleColor(buttonTextColour);
	geeseButton->setZoomScale(0.1f);
	geeseButton->setPosition(pos);
	geeseButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.geeseAi = false;
			options.foxAi = true;
			geeseButton->setBackgroundColour(GameSettings::selectedButtonColour);
			foxButton->setBackgroundColour(GameSettings::deselectedButtonColour);
		}
	});
	this->addChild(geeseButton);

	playAsButtonX += spacing + playAsButtonWidth;
	pos = Vec2(playAsButtonX, yPos);

	foxButton = SimpleButton::create();
	foxButton->addSimpleBackground(GameSettings::deselectedButtonColour, playAsButtonWidth, optionButtonHeight);
	foxButton->setTitleText("Fox");
	foxButton->setTitleFontSize(optionButtonFontSize);
	foxButton->setTitleFontName(GameSettings::font);
	foxButton->setTitleColor(buttonTextColour);
	foxButton->setZoomScale(0.1f);
	foxButton->setPosition(pos);
	foxButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.geeseAi = true;
			options.foxAi = false;
			foxButton->setBackgroundColour(GameSettings::selectedButtonColour);
			geeseButton->setBackgroundColour(GameSettings::deselectedButtonColour);
		}
	});
	this->addChild(foxButton);

	playAsLabel->setVisible(m_gameSingle);
	geeseButton->setVisible(m_gameSingle);
	foxButton->setVisible(m_gameSingle);

	// Geese
	yPos = yPos - optionButtonHeight * 1.4;
	auto geeseLabel = Label::createWithTTF("Number of Geese", GameSettings::font, GameSettings::buttonFontSize);
	geeseLabel->setPosition(Vec2(origin.x + visibleSize.width / 2, yPos));
	geeseLabel->setTextColor(subHeadingColour);
	this->addChild(geeseLabel, 1);

	yPos = yPos - geeseLabel->getContentSize().height - (optionButtonHeight / 2);

	float geeseButtonWidth = ((visibleSize.width * 0.9) - (spacing * 2)) / 3;
	float geeseButtonX = origin.x + (((visibleSize.width * 0.1) + geeseButtonWidth) / 2);
	pos = Vec2(geeseButtonX, yPos);

	thirteenButton = SimpleButton::create();
	thirteenButton->addSimpleBackground(GameSettings::selectedButtonColour, geeseButtonWidth, optionButtonHeight);
	thirteenButton->setTitleText("13");
	thirteenButton->setTitleFontSize(optionButtonFontSize);
	thirteenButton->setTitleFontName(GameSettings::font);
	thirteenButton->setTitleColor(buttonTextColour);
	thirteenButton->setZoomScale(0.1f);
	thirteenButton->setPosition(pos);
	thirteenButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.numberOfGeese = 13;
			thirteenButton->setBackgroundColour(GameSettings::selectedButtonColour);
			fifthteenButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			seventeenButton->setBackgroundColour(GameSettings::deselectedButtonColour);
		}
	});
	this->addChild(thirteenButton);

	geeseButtonX += spacing + geeseButtonWidth;
	pos = Vec2(geeseButtonX, yPos);

	fifthteenButton = SimpleButton::create();
	fifthteenButton->addSimpleBackground(GameSettings::deselectedButtonColour, geeseButtonWidth, optionButtonHeight);
	fifthteenButton->setTitleText("15");
	fifthteenButton->setTitleFontSize(optionButtonFontSize);
	fifthteenButton->setTitleFontName(GameSettings::font);
	fifthteenButton->setTitleColor(buttonTextColour);
	fifthteenButton->setZoomScale(0.1f);
	fifthteenButton->setPosition(pos);
	fifthteenButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.numberOfGeese = 15;
			thirteenButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			fifthteenButton->setBackgroundColour(GameSettings::selectedButtonColour);
			seventeenButton->setBackgroundColour(GameSettings::deselectedButtonColour);
		}
	});
	this->addChild(fifthteenButton);

	geeseButtonX += spacing + geeseButtonWidth;
	pos = Vec2(geeseButtonX, yPos);

	seventeenButton = SimpleButton::create();
	seventeenButton->addSimpleBackground(GameSettings::deselectedButtonColour, geeseButtonWidth, optionButtonHeight);
	seventeenButton->setTitleText("17");
	seventeenButton->setTitleFontSize(optionButtonFontSize);
	seventeenButton->setTitleFontName(GameSettings::font);
	seventeenButton->setTitleColor(buttonTextColour);
	seventeenButton->setZoomScale(0.1f);
	seventeenButton->setPosition(pos);
	seventeenButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.numberOfGeese = 17;
			thirteenButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			fifthteenButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			seventeenButton->setBackgroundColour(GameSettings::selectedButtonColour);
		}
	});
	this->addChild(seventeenButton);

	// Difficulty
	yPos = yPos - optionButtonHeight * 1.4;
	auto difficultyLabel = Label::createWithTTF("Difficulty", GameSettings::font, GameSettings::buttonFontSize);
	difficultyLabel->setPosition(Vec2(origin.x + visibleSize.width / 2, yPos));
	difficultyLabel->setTextColor(subHeadingColour);
	this->addChild(difficultyLabel, 1);

	yPos = yPos - difficultyLabel->getContentSize().height - (optionButtonHeight / 2);

	float difficultyButtonWidth = ((visibleSize.width * 0.9) - (spacing * 2)) / 3;
	float difficultyButtonX = origin.x + ( ((visibleSize.width * 0.1) + difficultyButtonWidth) / 2);
	pos = Vec2(difficultyButtonX, yPos);

	easyButton = SimpleButton::create();
	easyButton->addSimpleBackground(GameSettings::deselectedButtonColour, difficultyButtonWidth, optionButtonHeight);
	easyButton->setTitleText("Easy");
	easyButton->setTitleFontSize(optionButtonFontSize);
	easyButton->setTitleFontName(GameSettings::font);
	easyButton->setTitleColor(buttonTextColour);
	easyButton->setZoomScale(0.1f);
	easyButton->setPosition(pos);
	easyButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.difficulty = 0;
			easyButton->setBackgroundColour(GameSettings::selectedButtonColour);
			mediumButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			hardButton->setBackgroundColour(GameSettings::deselectedButtonColour);
		}
	});
	this->addChild(easyButton);

	difficultyButtonX += spacing + difficultyButtonWidth;
	pos = Vec2(difficultyButtonX, yPos);

	mediumButton = SimpleButton::create();
	mediumButton->addSimpleBackground(GameSettings::selectedButtonColour, difficultyButtonWidth, optionButtonHeight);
	mediumButton->setTitleText("Medium");
	mediumButton->setTitleFontSize(optionButtonFontSize);
	mediumButton->setTitleFontName(GameSettings::font);
	mediumButton->setTitleColor(buttonTextColour);
	mediumButton->setZoomScale(0.1f);
	mediumButton->setPosition(pos);
	mediumButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.difficulty = 1;
			easyButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			mediumButton->setBackgroundColour(GameSettings::selectedButtonColour);
			hardButton->setBackgroundColour(GameSettings::deselectedButtonColour);
		}
	});
	this->addChild(mediumButton);

	difficultyButtonX += spacing + difficultyButtonWidth;
	pos = Vec2(difficultyButtonX, yPos);

	hardButton = SimpleButton::create();
	hardButton->addSimpleBackground(GameSettings::deselectedButtonColour, difficultyButtonWidth, optionButtonHeight);
	hardButton->setTitleText("Hard");
	hardButton->setTitleFontSize(optionButtonFontSize);
	hardButton->setTitleFontName(GameSettings::font);
	hardButton->setTitleColor(buttonTextColour);
	hardButton->setZoomScale(0.1f);
	hardButton->setPosition(pos);
	hardButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			options.difficulty = 2;
			easyButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			mediumButton->setBackgroundColour(GameSettings::deselectedButtonColour);
			hardButton->setBackgroundColour(GameSettings::selectedButtonColour);
		}
	});
	this->addChild(hardButton);

	// Hide options for two player
	difficultyLabel->setVisible(m_gameSingle);
	easyButton->setVisible(m_gameSingle);
	mediumButton->setVisible(m_gameSingle);
	hardButton->setVisible(m_gameSingle);

	// Start button
	pos = Vec2(origin.x + (visibleSize.width / 2), buttonY);
	
	auto startButton = SimpleButton::create();
	startButton->addSimpleBackground(GameSettings::buttonColour, buttonWidth, buttonHeight);
	startButton->setTitleText("Start");
	startButton->setTitleFontSize(GameSettings::buttonFontSize);
	startButton->setTitleFontName(GameSettings::font);
	//startButton->setTitleColor(GameSettings::buttonTextColour);
	startButton->setZoomScale(0.1f);
	startButton->setPosition(pos);
	startButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED) {
			Director::getInstance()->replaceScene(
				TransitionFade::create(0.5, GameScene::createScene(options), Color3B(GameSettings::menuBackgroundColour)));
		}
	});
	this->addChild(startButton);

	return true;
}
