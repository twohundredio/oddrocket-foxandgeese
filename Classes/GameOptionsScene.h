#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "GameOptions.h"
#include "SimpleButton.h"

class GameOptionsScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene(bool singlePlayer);
	virtual bool init();
	CREATE_FUNC(GameOptionsScene);

private:
	GameOptions options;
	SimpleButton *geeseButton;
	SimpleButton *foxButton;
	SimpleButton *thirteenButton;
	SimpleButton *fifthteenButton;
	SimpleButton *seventeenButton;
	SimpleButton *easyButton;
	SimpleButton *mediumButton;
	SimpleButton *hardButton;
};

