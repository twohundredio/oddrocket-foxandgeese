#pragma once

#include <vector>
#include <random>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "FGBoard.h"
#include "GameOptions.h"
#include "GameSettings.h"
#include "SimpleButton.h"

class GameScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene(GameOptions options);
	virtual bool init();

	virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);
	CREATE_FUNC(GameScene);
	
private:
	std::random_device random_device;
	std::mt19937 random_engine;
	static const int nodeRadius = 18;
	std::vector<cocos2d::DrawNode*> nodes;
	cocos2d::DrawNode *moveNode;
	int selected;
	cocos2d::Label *turnLabel;
	cocos2d::Label *turnCountLabel;
	cocos2d::Label *timeCountLabel;
	cocos2d::LayerColor *msgLayer;
	cocos2d::Label *msgLabel;
	SimpleButton *finishButton;
	cocos2d::ui::Button *soundButton;
	static const cocos2d::Color4F nodeColour;
	static const cocos2d::Color4F foxColour;
	static const cocos2d::Color4F geeseColour;
	SimpleButton *turnButton;
	bool geeseAi;
	bool foxAi;
	bool showingAnimation;
	FGBoard::Move animateMove;
	int animationStep;
	int messageStep;
	GameSettings settings;
	std::vector<std::string> sounds;
	static constexpr float minMoveTime = 1.0f;

	void updateBoard();
	void startAnimation();
	void drawBoard(float x, float y, float size);
	void setNodeColour(cocos2d::DrawNode* node, cocos2d::Color4F colour);
	cocos2d::Vec2 getXYFromIndex(int i);
	int getNodeIdxAt(cocos2d::Vec2 location, float intersectRadius);
	int getIndexFromXY(int x, int y);
	void setTurnButtonVisable(bool visable);
	void setFinishButtonVisable(bool visable);
	void geeseCallback(float dt);
	void foxCallback(float dt);
	void animateCallback(float dt);
	void playMoveSound();
	void showMessage(std::string& msg);
	void messageCallback(float dt);
	void showMoveResult(FGBoard::MoveResult result);

};

