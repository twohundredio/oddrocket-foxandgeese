#include <algorithm>
#include <cassert>
#include <cmath>
#include <random>
#include <iostream>
#include <iterator>
#include <string>
#include <sstream>
#include <vector>
#include <stack>
#include "FGBoard.h"
#include "PatchNDK.h"
#include "cocos2d.h"

using std::stringstream;
using std::endl;
using std::vector;
using std::stack;

FGBoard::FGBoard()
{
	setupBoard(13);
    m_difficulty = 0;
	std::random_device rd;
	generator.seed(rd());
}

FGBoard::~FGBoard()
{
}

void FGBoard::setupBoard(int numberOfGeese)
{
	for (int y = 0; y < 7; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			if ((x < 2 || x > 4) && (y < 2 || y > 4))
			{
				board[x][y] = PieceType::NOGO;
			}
			else
			{
				board[x][y] = PieceType::NONE;

				if (y <= 2)
				{
					board[x][y] = PieceType::GEESE;
				}
			}
		}
	}

	board[3][4] = PieceType::FOX;

	if (numberOfGeese >= 15) {
		board[0][3] = PieceType::GEESE;
		board[6][3] = PieceType::GEESE;
	}
	if (numberOfGeese >= 17) {
		board[1][3] = PieceType::GEESE;
		board[5][3] = PieceType::GEESE;
	}

	geeseTurn = true;
	hasMoved = false;
	foxJumped = false;
	gameOver = false;

	numberOfMoves = 0;
	startTime = std::chrono::system_clock::now();
}

void FGBoard::setDifficulty(int difficulty)
{
    m_difficulty = difficulty;
}

std::vector<double> FGBoard::getRepresentation()
{
    std::vector<double> output;

    for (int y = 0; y < 7; y++)
    {
        for (int x = 0; x < 7; x++)
        {
            if (board[x][y] == PieceType::FOX)
            {
                output.push_back(-1);
            }
            else if (board[x][y] == PieceType::GEESE)
            {
                output.push_back(1);
            }
            else if (board[x][y] == PieceType::NONE)
            {
                output.push_back(0);
            }
        }
    }

    return output;
}

bool FGBoard::isGameOver()
{
	return gameOver;
}

int FGBoard::getNumberOfMoves()
{
	return numberOfMoves;
}

std::string FGBoard::getPlayTime()
{
	std::chrono::duration<double> elapsed_seconds = endTime - startTime;
	int min = elapsed_seconds.count() / 60;
	int seconds = ((unsigned int)elapsed_seconds.count()) % 60;
	return patchndk::to_string(min).append(" minutes ").append(patchndk::to_string(seconds)).append(" seconds");
}

FGBoard::PieceType FGBoard::getPeiceAt(int x, int y)
{
	return board[x][y];
}

FGBoard::MoveResult FGBoard::isValidMove(int sx, int sy, int ex, int ey, bool isGeeseMove, bool foxJumped) const
{
	MoveResult isValid;
	isValid.type = MoveType::INVALID;
	isValid.reason = MoveReason::OK;

	if (ex < 0 || ey < 0 || ex > 6 || ey > 6 ||
		board[ex][ey] == PieceType::NOGO)
	{
		isValid.type = MoveType::INVALID;
		isValid.reason = MoveReason::BOARD;
		return isValid;
	}

	if ((isGeeseMove && board[sx][sy] != PieceType::GEESE) ||
		(!isGeeseMove && board[sx][sy] != PieceType::FOX))
	{
		isValid.type = MoveType::INVALID;
		isValid.reason = MoveReason::TURN;
		return isValid;
	}

	if (board[ex][ey] == PieceType::NONE)
	{
		int xd = abs(ex - sx);
		int yd = abs(ey - sy);
		bool canMoveDiagonal = (sx % 2 == 0 && sy % 2 == 0) || (sx % 2 == 1 && sy % 2 == 1);

		if ((xd == 1 && yd == 0) ||
			(xd == 0 && yd == 1) ||
			(xd == 1 && yd == 1 && canMoveDiagonal))
		{
			if (!foxJumped)
			{
				isValid.type = MoveType::MOVE;
			}
			else
			{
				isValid.type = MoveType::INVALID;
				isValid.reason = MoveReason::JUMPED;
			}
		}
		else if (!isGeeseMove)
		{
			// Check for jump
			if ((xd == 2 && yd == 0) ||
				(xd == 0 && yd == 2) ||
				(xd == 2 && yd == 2 && canMoveDiagonal))
			{
				int xj = sx + ((ex - sx) / 2);
				int yj = sy + ((ey - sy) / 2);

				if (board[xj][yj] == PieceType::GEESE)
				{
					isValid.type = MoveType::JUMP;
				}
				else
				{
					isValid.type = MoveType::INVALID;
					isValid.reason = MoveReason::NOTHINGJUMPED;
				}
			}
			else
			{
				isValid.type = MoveType::INVALID;
				if (((!canMoveDiagonal) && (xd == yd))
					|| (xd != 0 && yd != 0 && yd != xd))
				{
					isValid.reason = MoveReason::LINE;
				}
				else
				{
					isValid.reason = MoveReason::DISTANCE;
				}
			}
		}
		else
		{
			isValid.type = MoveType::INVALID;
			if (((!canMoveDiagonal) && (xd == yd))
				|| (xd != 0 && yd != 0 && yd != xd))
			{
				isValid.reason = MoveReason::LINE;
			}
			else
			{
				isValid.reason = MoveReason::DISTANCE;
			}
		}
	}
	else 
	{
		isValid.type = MoveType::INVALID;
		isValid.reason = MoveReason::OCCUPIED;
	}

	return isValid;
}

void FGBoard::performMove(int sx, int sy, int ex, int ey)
{
	assert(board[sx][sy] != PieceType::NONE);
	assert(board[sx][sy] != PieceType::NOGO);

	board[ex][ey] = board[sx][sy];
	board[sx][sy] = PieceType::NONE;

	hasMoved = true;
	numberOfMoves++;

	if ((!(foxJumped && foxHasJumps())) || (!geeseTurn && hasFoxWon()))
	{
		endTurn();
	}
}

void FGBoard::performJump(int sx, int sy, int ex, int ey)
{
	assert(board[sx][sy] == PieceType::FOX);

	int xj = sx + ((ex - sx) / 2);
	int yj = sy + ((ey - sy) / 2);

	foxJumped = true;

	board[xj][yj] = PieceType::NONE;
	performMove(sx, sy, ex, ey);
}

bool FGBoard::isGeeseTurn()
{
	return geeseTurn;
}

FGBoard::MoveResult FGBoard::tryMove(int sx, int sy, int ex, int ey)
{
	FGBoard::MoveResult move = isValidMove(sx, sy, ex, ey, geeseTurn, foxJumped);
	
	if (move.type != FGBoard::MoveType::INVALID)
	{
		if (move.type == FGBoard::MoveType::MOVE)
		{
			performMove(sx, sy, ex, ey);
		}
		else if (move.type == FGBoard::MoveType::JUMP)
		{
			performJump(sx, sy, ex, ey);
		}
	}

	return move;
}

FGBoard::XYPos FGBoard::findFox() const
{
	XYPos pos;
	pos.x = -1;
	pos.y = -1;

	for (int y = 0; y < 7; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			if (board[x][y] == PieceType::FOX)
			{
				pos.x = x;
				pos.y = y;
				return pos;
			}
		}
	}

	return pos;
}

std::vector<FGBoard::Move> FGBoard::getFoxMoves() const
{
	std::vector<Move> moves;
	XYPos pos = findFox();
	int sx = pos.x;
	int sy = pos.y;
	
	for (int x = -2; x <= 2; x++)
	{
		for (int y = -2; y <= 2; y++)
		{
			int ex = sx + x;
			int ey = sy + y;

			if (isValidMove(sx, sy, ex, ey, false, foxJumped).type != MoveType::INVALID)
			{
				Move m;
				m.valid = true;
				m.jump = false;
				m.ex = ex;
				m.ey = ey;
				m.sx = sx;
				m.sy = sy;

				if (x == 2 || y == 2 || x == -2 || y == -2)
				{
					m.jump = true;
				}

				moves.push_back(m);
			}
		}
	}
	return moves;
}

bool FGBoard::foxHasJumps() const
{
	XYPos pos = findFox();
	int sx = pos.x;
	int sy = pos.y;

	for (int x = -2; x <= 2; x++)
	{
		for (int y = -2; y <= 2; y++)
		{
			int ex = sx + x;
			int ey = sy + y;

			if (isValidMove(sx, sy, ex, ey, false, false).type != MoveType::INVALID)
			{
				if (x == 2 || y == 2 || x == -2 || y == -2)
				{
					return true;
				}
			}
		}
	}

	return false;
}

std::vector<FGBoard::XYPos> FGBoard::findGeese() const
{
	std::vector<FGBoard::XYPos> geese;

	for (int y = 0; y < 7; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			if (board[x][y] == PieceType::GEESE)
			{
				XYPos p;
				p.x = x;
				p.y = y;

				geese.push_back(p);
			}
		}
	}

	return geese;
}

std::vector<FGBoard::Move> FGBoard::getGeeseMoves() const
{ 
	std::vector<Move> moves;
	std::vector<XYPos> geese = findGeese();
	
	for (auto g : geese)
	{
		for (int x = -1; x <= 1; x++)
		{
			for (int y = -1; y <= 1; y++)
			{
				int sx = g.x;
				int sy = g.y;
				int ex = sx + x;
				int ey = sy + y;

				if (isValidMove(sx, sy, ex, ey, true, false).type != MoveType::INVALID)
				{
					Move m;
					m.valid = true;
					m.jump = false;
					m.ex = ex;
					m.ey = ey;
					m.sx = sx;
					m.sy = sy;

					moves.push_back(m);
				}
			}
		}
	}

	return moves;
}

FGBoard::Move FGBoard::getAiMove()
{
	Move m;
	vector<FGBoard::Move> moves;
	FGBoard fgboard;
	int best = 0;
	int selectFrom = 0;
    int selectFromRnd = 0;
    int selectFromTop = 0;
	int numberOfTop = 1; // Take top X scores
    int numberOfTopOrig = 1;
    int depth = 3;
    double consider = 0;

	fgboard.setupBoard(13);

    if (m_difficulty == 0)
    {
        // Set easy
        consider = 0.5;
        numberOfTop = 3;
        depth = 3;
    }
    else if (m_difficulty == 1)
    {
        // Set medium
        numberOfTop = 2;
        depth = 3;
    }
    else
    {
        // Set hard
        numberOfTop = 1;
        depth = 4;
    }
    numberOfTopOrig = numberOfTop;

	vector<Move> r = minmax(fgboard, moves, depth);

    if (consider > 0)
    {
        selectFromRnd = std::max(1.0, r.size() * consider);
    }
    
    best = r.at(0).score;
    for (auto result : r)
    {
        if (result.score != best)
        {
            --numberOfTop;
            if (numberOfTop <= 0)
            {
                break;
            }
            else
            {
                best = result.score;
            }
        }

        if (result.score == best)
        {
            selectFromTop++;
        }
    }
    
    selectFrom = std::max(selectFromRnd, selectFromTop);

	std::uniform_int_distribution<int> distribution(0, selectFrom - 1);
	int moveselection = distribution(generator);
	m = r.at(moveselection);

    if (false)
    {
        stringstream ss;
        ss << "Difficulty " << m_difficulty << " Consider " << consider
            << " numberOfTop " << numberOfTopOrig << " Depth " << depth << endl;
        ss << "Select from " << selectFrom << " selectFromTop " << selectFromTop
            << " selectFromRnd " << selectFromRnd << endl;
        ss << "moveselection " << moveselection << " results size " << r.size() << endl;
        for (auto mo : r)
        {
            ss << mo.score << " From " << mo.sx << "," << mo.sy << " To " << mo.ex << "," << mo.ey << " Jump " << mo.jump << endl;
        }

        cocos2d::log("%s", ss.str().c_str());
    }

	return m;
}

FGBoard::Move FGBoard::moveGeese()
{
	Move m;
    assert(isGeeseTurn());
	m = getAiMove();
	return m;
}

FGBoard::Move FGBoard::moveFox()
{
	Move m;
    assert(!isGeeseTurn());
	m = getAiMove();
	return m;
}

bool FGBoard::hasFoxWon() const
{
	if (countGeese() <= 4)
	{
		return true;
	}

	return false;
}

bool FGBoard::foxCanEndTurn()
{
	return foxJumped;
}

int FGBoard::countGeese() const
{
	int geese = 0;

	for (int y = 0; y < 7; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			if (board[x][y] == PieceType::GEESE)
			{
				geese++;
			}
		}
	}

	return geese;
}

unsigned int FGBoard::numberOfFoxMoves() const
{
	unsigned int moves = 0;
	XYPos pos = findFox();
	int sx = pos.x;
	int sy = pos.y;

	for (int x = -2; x <= 2; x++)
	{
		for (int y = -2; y <= 2; y++)
		{
			int ex = sx + x;
			int ey = sy + y;

			if (isValidMove(sx, sy, ex, ey, false, false).type != MoveType::INVALID)
			{
				moves++;
			}
		}
	}

	return moves;
}

unsigned int FGBoard::numberOfVulnerableGeese() const
{
	unsigned int count = 0;
	std::vector<XYPos> geese = findGeese();

	for (auto g : geese)
	{
		bool canMoveDiagonal = (g.x % 2 == 0 && g.y % 2 == 0) || (g.x % 2 == 1 && g.y % 2 == 1);
		int checks = canMoveDiagonal ? 4 : 2;

		for (int i = 0; i < checks; i++)
		{
			int x1;
			int y1;
			int x2;
			int y2;

			if (i == 0)
			{
				x1 = g.x - 1;
				y1 = g.y;
				x2 = g.x + 1;
				y2 = g.y;
			}
			else if (i == 1)
			{
				x1 = g.x;
				y1 = g.y - 1;
				x2 = g.x;
				y2 = g.y + 1;
			}
			else if (i == 2)
			{
				x1 = g.x - 1;
				y1 = g.y - 1;
				x2 = g.x + 1;
				y2 = g.y + 1;
			}
			else
			{
				x1 = g.x - 1;
				y1 = g.y + 1;
				x2 = g.x + 1;
				y2 = g.y - 1;
			}

			if (x1 >= 0 && y1 >= 0 && x2 <= 6 && y2 <= 6)
			{
				if (board[x1][y1] == PieceType::NONE && board[x2][y2] == PieceType::NONE)
				{
					count++;
					break;
				}
			}
		}
	}

	return count;
}

double FGBoard::getFoxGeeseDistance() const
{
	XYPos foxPos = findFox();
	
	std::vector<XYPos> geese = findGeese();
	int count = 0;
	double diffTotal = 0;

	for (auto g : geese)
	{
		count++;

		double xd = abs(foxPos.x - g.x);
		double yd = abs(foxPos.y - g.y);

		diffTotal += sqrt(pow(xd, 2) + (yd, 2));
	}

	double a2 = diffTotal / count;
	return a2;
}


bool FGBoard::haveGeeseWon() const
{
	return numberOfFoxMoves() == 0;
}

void FGBoard::endTurn()
{
	hasMoved = false;
	foxJumped = false;

	if (haveGeeseWon() || hasFoxWon())
	{
		if (!gameOver)
		{
			endTime = std::chrono::system_clock::now();
		}
		gameOver = true;
	}

	geeseTurn = !geeseTurn;
}

void FGBoard::copyState(FGBoard &fgboard)
{
	// Copy board
	for (int y = 0; y < 7; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			board[x][y] = fgboard.board[x][y];
		}
	}

	geeseTurn = fgboard.geeseTurn;
	hasMoved = fgboard.hasMoved;
	foxJumped = fgboard.foxJumped;
	gameOver = fgboard.gameOver;
    m_difficulty = fgboard.m_difficulty;
}

bool FGBoard::evalCanMakeMove(FGBoard::PieceType evalboard[][7], int sx, int sy, int ex, int ey)
{
    if (sx >= 0 && sy >= 0 && sx < 7 && sy < 7 && evalboard[ex][ey] == FGBoard::PieceType::NONE)
    {
        int xd = abs(ex - sx);
        int yd = abs(ey - sy);
        bool canMoveDiagonal = (sx % 2 == 0 && sy % 2 == 0) || (sx % 2 == 1 && sy % 2 == 1);

        if ((xd == 1 && yd == 0) ||
            (xd == 0 && yd == 1) ||
            (xd == 1 && yd == 1 && canMoveDiagonal))
        {
                return true;
        }

        if ((xd == 2 && yd == 0) ||
            (xd == 0 && yd == 2) ||
            (xd == 2 && yd == 2 && canMoveDiagonal))
        {
            int xj = sx + ((ex - sx) / 2);
            int yj = sy + ((ey - sy) / 2);

            if (evalboard[ex][ey] == FGBoard::PieceType::NONE)
            {
                    return true;
            }
        }
    }

    return false;
}

int FGBoard::evalBoard(FGBoard &fgboard, std::vector<FGBoard::Move> &moves)
{
	int score = 0;
    XYPos foxpos = fgboard.findFox();
    int zone[7] = { };
    int geeseCount = 0;
    enum PieceType connectedboard[7][7]; // Is connected?
    stack<XYPos> posStack;
    int accessable = 0;
    double yavg = 0;

    // Start with fox
    // Try each node to see if connected to the next
    // Use number of nodes accessable in score
    for (int y = 0; y < 7; y++)
    {
        for (int x = 0; x < 7; x++)
        {
            connectedboard[x][y] = fgboard.board[x][y];
            if (connectedboard[x][y] == PieceType::GEESE)
            {
                yavg += y;
                geeseCount++;
            }
        }
    }

    yavg = yavg / geeseCount;
   
    // Count accessable squares
    posStack.push(foxpos);
    while (!posStack.empty())
    {
        XYPos cpos = posStack.top();
        posStack.pop();
        for (int x = -2; x <= 2; x++)
        {
            for (int y = -2; y <= 2; y++)
            {
                int sx = cpos.x;
                int sy = cpos.y;
                int ex = sx + x;
                int ey = sy + y;

                if (evalCanMakeMove(connectedboard, sx, sy, ex, ey))
                {
                    XYPos p;
                    p.x = ex;
                    p.y = ey;
                    posStack.push(p);
                    accessable++;
                    connectedboard[ex][ey] = PieceType::NOGO;
                }
            }
        }
    }

    score += (33 - accessable) * 100;

    // Make geese move towards fox
    XYPos sPos;
    int xlook = 6;
    int ylook = 6;
    if (foxpos.x <= 1)
    {
        sPos.x = 0;
        sPos.y = 3;
        xlook = 4;
        ylook = 2;
    }
    else if (foxpos.x >= 5)
    {
        sPos.x = 6;
        sPos.y = 3;
        xlook = 4;
        ylook = 2;
    }
    else if (foxpos.y <= 1)
    {
        sPos.x = 3;
        sPos.y = 0;
        xlook = 2;
        ylook = 4;
    }
    else if (foxpos.y >= 5)
    {
        sPos.x = 3;
        sPos.y = 6;
        xlook = 2;
        ylook = 4;
    }
    else
    {
        sPos.x = 3;
        sPos.y = 3;
        xlook = 2;
        ylook = 2;
    }

    for (int y = -ylook; y <= ylook; y++)
    {
        for (int x = -xlook; x <= xlook; x++)
        {
            int fx = sPos.x + x;
            int fy = sPos.y + y;

            if (fx >= 0 && fy >= 0 && fx < 7 && fy < 7)
            {
                if (fgboard.board[fx][fy] == PieceType::GEESE)
                {
                    int z = std::max(abs(y), abs(x));
                    zone[z]++;
                }
            }
        }
    }

    for (int n = 0; n < 7; n++)
    {
        int zmult = (10 - n);
        score += zone[n] * zmult;
    }

    // Check for win
    if (fgboard.haveGeeseWon())
    {
        score = AISCOREWIN;
        score -= moves.size();
    }
    else if (fgboard.hasFoxWon())
    {
        score = -AISCOREWIN;
        score += moves.size();
    }

    return score;
}

std::vector<FGBoard::Move> FGBoard::minmax(FGBoard &fgboard, vector<FGBoard::Move> &moves, int depth)
{
	vector<Move> results;
	Move pm;
	pm.valid = false;
	bool maxGeese;

    assert(depth > 0);
    assert(!fgboard.isGameOver());
    assert(moves.empty());

	// Setup baord
	fgboard.copyState(*this);

	--depth;

	std::vector<Move> playableMoves;
	if (fgboard.isGeeseTurn())
	{
		playableMoves = fgboard.getGeeseMoves();
		maxGeese = true;
	}
	else
	{
		playableMoves = fgboard.getFoxMoves();
		maxGeese = false;
	}

	assert(playableMoves.size() > 0);

    for (auto m : playableMoves)
    {
        moves.push_back(m);
        Move r = minmax_single(fgboard, moves, depth);
        moves.pop_back();
        results.push_back(r);
    }

	if (maxGeese)
	{
		std::sort(std::begin(results), std::end(results),
			[](const Move & a, const Move & b)
			{
				return a.score > b.score;
			});
	}
	else
	{
		std::sort(std::begin(results), std::end(results), 
			[](const Move & a, const Move & b)
			{
				return a.score < b.score;
			});
	}

	return results;
}

FGBoard::Move FGBoard::minmax_single(FGBoard &fgboard, std::vector<FGBoard::Move> &moves, int depth)
{
    int score;
    Move pm;
    pm.valid = false;
    bool maxGeese;
    std::vector<Move> playableMoves;

    bool undoGeeseTurn = fgboard.geeseTurn;
    bool undohasMoved = fgboard.hasMoved;
    bool undofoxJumped = fgboard.foxJumped;
    bool undogameOver = fgboard.gameOver;

    assert(!fgboard.isGameOver());

    // Perform move
    Move currentMove = moves.back();
    MoveResult r = fgboard.tryMove(currentMove.sx, currentMove.sy, currentMove.ex, currentMove.ey);
    assert(r.type != MoveType::INVALID);

    score = evalBoard(fgboard, moves);

    if (depth <= 0 || fgboard.isGameOver())
    {
        if (moves.size() > 0)
        {
            pm = moves.at(0);
            pm.score = score;
        }
        goto undo;
    }

    --depth;

    if (fgboard.isGeeseTurn())
    {
        playableMoves = fgboard.getGeeseMoves();
        maxGeese = true;
    }
    else
    {
        playableMoves = fgboard.getFoxMoves();
        maxGeese = false;
    }

    assert(playableMoves.size() > 0);

    if (maxGeese)
    {
        score = -AISCOREINFINITY;
        for (auto m : playableMoves)
        {
            moves.push_back(m);
            Move r = minmax_single(fgboard, moves, depth);
            moves.pop_back();
            if (r.score > score)
            {
                score = r.score;
                pm = r;
            }
        }
    }
    else
    {
        score = AISCOREINFINITY;
        for (auto m : playableMoves)
        {
            moves.push_back(m);
            Move r = minmax_single(fgboard, moves, depth);
            moves.pop_back();
            if (r.score < score)
            {
                score = r.score;
                pm = r;
            }
        }
    }

undo:
    // Undo move
    fgboard.geeseTurn = undoGeeseTurn;
    fgboard.hasMoved =  undohasMoved;
    fgboard.foxJumped = undofoxJumped;
    fgboard.gameOver = undogameOver;

    assert(fgboard.board[currentMove.sx][currentMove.sy] == PieceType::NONE);
    fgboard.board[currentMove.sx][currentMove.sy] = fgboard.board[currentMove.ex][currentMove.ey];
    fgboard.board[currentMove.ex][currentMove.ey] = PieceType::NONE;

    if (currentMove.jump)
    {
        assert(fgboard.board[currentMove.sx][currentMove.sy] == PieceType::FOX);

        int xj = currentMove.sx + ((currentMove.ex - currentMove.sx) / 2);
        int yj = currentMove.sy + ((currentMove.ey - currentMove.sy) / 2);

        fgboard.board[xj][yj] = PieceType::GEESE;
    }

    return pm;
}