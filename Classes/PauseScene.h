#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "GameOptions.h"

class PauseScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene(GameOptions options);
	virtual bool init();
	CREATE_FUNC(PauseScene);
};

