#pragma once
#include <FGBoard.h>

class BackgroundAI
{
public:
	BackgroundAI();
	~BackgroundAI();

	void startAIMove(FGBoard &board);
	void aiCallback(void* v);
	bool moveAvailable();
	FGBoard::Move getMove();

private:
	struct air_s {
		unsigned int gen;
		FGBoard board;
		FGBoard::Move result;
	};

	unsigned int m_gen;
	bool m_moveAvailable;
	FGBoard::Move m_result;
};

